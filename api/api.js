var initBets  = require("./bets");
var initStats = require("./stats");
var initUsers = require("./users");
var initTransactions = require("./transactions");
const authAdminMiddleware = require('../middlewares/authAdmin');
const authMiddleware = require('../middlewares/auth')
const betValidation = require('../middlewares/betValidation')
const initEvents = require('./events');

module.exports = (app) => {
    app.use('/bets',betValidation, authMiddleware, initBets);
    app.use('/stats', authAdminMiddleware, initStats);
    app.use('/users', initUsers);
    app.use('/transactions',authMiddleware, initTransactions);
    app.use('/events', initEvents);
    app.get("/health", (req, res) => {
        res.send("Hello World!");
      });
  };