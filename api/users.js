const { Router } = require('express');
const router = Router();
const { userIdValidation, userPost } = require('../middlewares/userValidation');
const authMiddleware = require('../middlewares/auth')

router.get("/:id", userIdValidation, (req, res) => {
    try {
        db("user").where('id', req.params.id).returning("*").then(([result]) => {
            if (!result) {
                res.status(404).send({ error: 'User not found' });
                return;
            }
            return res.send({
                ...result,
            });
        });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server Error");
        return;
    }
});

router.post("/", userPost, (req, res) => {

    req.body.balance = 0;
    db("user").insert(req.body).returning("*").then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;
        statEmitter.emit('newUser');
        return res.send({
            ...result,
            accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET)
        });
    }).catch(err => {
        if (err.code == '23505') {
            res.status(400).send({
                error: err.detail
            });
            return;
        }
        res.status(500).send("Internal Server Error");
        return;
    });
});

router.put("/:id", authMiddleware, userPost, (req, res) => {
    if (req.params.id !== tokenPayload.id) {
        return res.status(401).send({ error: 'UserId mismatch' });
    }
    db("user").where('id', req.params.id).update(req.body).returning("*").then(([result]) => {
        return res.send({
            ...result,
        });
    }).catch(err => {
        if (err.code == '23505') {
            console.log(err);
            res.status(400).send({
                error: err.detail
            });
            return;
        }
        console.log(err);
        res.status(500).send("Internal Server Error");
        return;
    });
});

module.exports = router;