var express = require("express");
var knex = require("knex");
var jwt = require("jsonwebtoken");
var joi = require('joi');
var EventEmitter = require('events');
var initApi = require('./api/api');


var dbConfig = require("./knexfile");
var app = express();

var port = 3000;

var statEmitter = new EventEmitter();
var stats = {
  totalUsers: 3,
  totalBets: 2,
  totalEvents: 3,
};

var db;
app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db = knex(dbConfig.development);
  db.raw('select 1+1 as result').then(function () {
    neededNext();
  }).catch(() => {
    throw new Error('No db connection');
  });
});

initApi(app);



app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    db.close(false, ()=>{
      console.log('DB connection is closed');
    });
    process.exit(0);
  });
});

// Do not change this line
module.exports = { app };