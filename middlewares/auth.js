module.exports = (req, res, next) => {
    let token = req.headers['authorization'];
      if(!token) {
        return res.status(401).send({ error: 'Not Authorized' });
      }
    next();  
}